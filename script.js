"use strict";

function UlList() {
	var that = this;

	this.list = document.createElement("UL");
	document.body.appendChild(this.list);

	var input = document.createElement("INPUT");
	input.type = "text";
	this.list.appendChild(input);
		

	this.addItem = function(text) {
		var li = new addlistItem(text);
		this.list.appendChild(li.listItem);
	}
		
	this.list.appendChild(createAdd());		
	this.list.appendChild(deleteAll());

	function createAdd() {
		var add = document.createElement("BUTTON");
		add.innerText = "Add";
		add.onclick = function() {
			that.addItem(input.value);
			input.value = "";
		}
		return add;
	}

	function deleteAll() {
		var deleteButton = document.createElement("BUTTON");
		deleteButton.innerText = "Delete list";
		deleteButton.onclick = function() {
			that.list.remove();
		}
		return deleteButton;
	}
	return this.list;
}





function addlistItem(text) {
	var that = this;
	this.listItem = createLi();
	
	var that = this;
	

	function createLi() {
			var item = document.createElement("LI");
			var label = document.createElement("LABEL");
			label.innerText = text;
			label.className = "edit";
			item.appendChild(label);
			item.appendChild(createInput());
			item.appendChild(createCheck());
			item.appendChild(createEditButt());
			item.appendChild(createDelete());
			item.appendChild(createNewListButton());
			
			return item;
	}

	function createInput() {
			var input = document.createElement("INPUT");
			input.type = "text";
			return input;
	}

	function createCheck() {
		var check = document.createElement("INPUT");
		check.type = "checkbox";
		check.onchange = function(){	
			that.listItem.classList.toggle("checked");
		}
		return check;
	}

	function createDelete() {
		var deleteButton = document.createElement("BUTTON");
		deleteButton.innerText = "Delete item";
		deleteButton.onclick = function() {
			that.listItem.remove();
		}
		return deleteButton;
	}

	function createNewListButton() {
		var newButton = document.createElement("BUTTON");
		newButton.innerText = "New List";
		newButton.onclick = function() {
			that.listItem.appendChild(new UlList());
		}
		return newButton;
	}

	function createEditButt() {
		var editButton = document.createElement("BUTTON");
		editButton.innerText = "Edit";
		editButton.onclick = function() {
			var edit = editButton.parentNode.firstChild;
			var containsClass = edit.classList.contains("edit");
			var input = editButton.previousSibling.previousSibling;
			if (containsClass) {
				input.value = edit.innerText;
				input.focus();
				edit.innerText = "";	
				editButton.innerText = "Save";
				
			} else {
				editButton.innerText = "Edit";
				edit.innerText = input.value;
				input.value = "";
			}
			edit.classList.toggle("edit");
		}
		return editButton;
	}
}
	

var createList = new UlList();

